# Yamagi Quake II

Yamagi Quake II is an enhanced client for id Software's Quake II with focus on offline and coop gameplay. Both the gameplay and the graphics are unchanged, but many bugs in the last official release were fixed and some nice to have features like widescreen support and a modern OpenGL 3.2 renderer were added. Unlike most other Quake II source ports Yamagi Quake II is fully 64-bit clean. It works perfectly on modern processors and operating systems. Yamagi Quake II runs on nearly all common platforms; including FreeBSD, Linux, NetBSD, OpenBSD, Windows and macOS (experimental)

Under GPL 2 license.

Website: https://www.yamagi.org/quake2/
Source Code: https://github.com/yquake2/yquake2
AppImage: https://gitlab.com/ose-appimages/yamagi-quake2/-/packages/2655716

## How to run:

* Download the AppImage and give it execution permissions.
* Get a **legal copy** of the game on (Steam)[https://store.steampowered.com/app/2320/QUAKE_II/], (GoG)[https://www.gog.com/game/quake_ii_quad_damage] or [Bethesda](https://bethesda.net/en/store/product/QU2CSTPCBG01)
* Copy the folder **baseq2** to the folder where you downloaded the AppImage, or vice versa. The AppImage must be **outside** the folder (see screenshot). ![yquake2-folder](yquake2-folder.png)
* Double ckick n the AppImage and enjoy ;)
